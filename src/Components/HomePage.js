import './css/Component.css';
import React from 'react';
import Header from './Header';
import Container from 'react-bootstrap/Container';

export default function HomePage() {

    return (
        <div>
            <Header Title={'Home page'} />
            <Container>
                To login successfully please use:<br />
                <b>email: eve.holt@reqres.in</b><br />
                <b>password: cityslicka</b><br />
                on login page
            </Container>
        </div>
    );
}