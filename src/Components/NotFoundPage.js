import './css/Component.css';
import React from "react";
import logo from './Image/404.png';

import Image from 'react-bootstrap/Image';

export default function NotFoundPage() {
    return (
            <Image fluid src={logo} className="bg w-100 "/>
    );
}  