import './css/Component.css';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import AuthActions from '../Actions/AuthActions'

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import { LinkContainer } from 'react-router-bootstrap'

export default function Header(props) {
    const dispatch = useDispatch();
    const stateObject = useSelector(state => state)

    return (
        <div>
            <Navbar bg="dark text-light p-3" expand="lg">
                <Container>
                    <Navbar.Brand href="" className='fw-bold text-light' >Homework</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav ">
                        <Nav className="d-flex w-100">
                            <LinkContainer to="/homepage">
                                <Nav.Link className='text-light'>Homepage</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to="/login">
                                <Nav.Link className='text-light'>Login</Nav.Link>
                            </LinkContainer>
                            <LinkContainer to="/register">
                                <Nav.Link className='text-light'>Register</Nav.Link>
                            </LinkContainer>
                        </Nav>
                        <Nav>
                            <Nav.Link className='text-light justify-content-end' onClick={() => dispatch(AuthActions.logOut())}>Logout</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

        </div>
    );
}